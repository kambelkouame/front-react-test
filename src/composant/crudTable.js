import React, { Component } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Toolbar } from 'primereact/toolbar';
import { InputText } from 'primereact/inputtext';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { Dialog } from 'primereact/dialog';
import './tableStyle.css';
export class CrudTable extends Component {

    constructor(props) {
        super(props);

        this.state = {
          
            displayBasic: false,
            displayBasic2: false,
            displayModal: false,
            displayMaximizable: false,
            displayPosition: false,
            displayResponsive: false,
            position: 'center'
        };
        this.onClick = this.onClick.bind(this);
        this.onHide = this.onHide.bind(this);
        this.actionBodyTemplate = this.actionBodyTemplate.bind(this);
        //this.actionBodyTemplate = this.actionBodyTemplate.bind(this);
      //  this.productService = new ProductService();
    }
/*
    onClick(name, position) {
        let state = {
            [`${name}`]: true
        };

        if (position) {
            state = {
                ...state,
                position
            }
        }

        this.setState(state);
    }

    onHide(name) {
        this.setState({
            [`${name}`]: false
        });
    }
*/
    renderFooter(name) {
        return (
            <div>
                <Button label="No" icon="pi pi-times" onClick={() => this.onHide(name)} className="p-button-text" />
                <Button label="Yes" icon="pi pi-check" onClick={() => this.onHide(name)} autoFocus />
            </div>
        );
    }


    componentDidMount() {
     //   this.productService.getProductsSmall().then(data => this.setState({ products: data }));
    }


    actionBodyTemplate() {
        return (
            <Button type="button" icon="pi pi-cog" className="p-button-secondary"></Button>
        );
    }


    addClient(){

    }
        rightToolbarTemplate() {
            return (
                <React.Fragment>
                    <Button label="New client" icon="pi pi-plus" className="p-button-help" onClick={() => this.addClient()} />
                </React.Fragment>
            )
        }
    render() {

        const header = (
            <div className="table-header">
                
                <h2>Client management</h2>
            
                <span className="p-input-icon-left">
                    <i className="pi pi-search" />
                    <InputText type="search" value={this.state.globalFilter} onChange={(e) => this.setState({ globalFilter: e.target.value })} placeholder="Search" />
                </span>
            </div>
        );

        return (
            <div>
                <br/><br/><br/><br/>
                <div className="card">
                <Toolbar className="p-mb-4"  right={this.rightToolbarTemplate}></Toolbar>
                    <DataTable    header={header} className="p-datatable-customers" footer="Footer" showGridlines>
                        <Column field="Name" header="Name" sortable></Column>
                        <Column field="Email" header="Email" sortable></Column>
                        <Column field="Phone" header="Phone" sortable></Column>
                        <Column field="Providers" header="Providers"></Column>
                        <Column body={this.actionBodyTemplate}></Column>
                    </DataTable>
                </div>

                <Dialog header="Header" visible={this.state.displayResponsive} onHide={() => this.ferm('displayResponsive')} breakpoints={{'960px': '75vw'}} style={{width: '50vw'}} footer={this.renderFooter('displayResponsive')}>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </Dialog>
            </div>
        );
    }
}
export default CrudTable;